package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

// Counter counts the events fired
var Counter int = 0

// Event is the event object
type Event struct {
	Time   time.Time `json:"time"`
	Ticker time.Time `json:"ticker"`
	Event  string    `json:"event"`
}

func main() {
	for {
		tickerTime := time.Now()
		fmt.Println("Tick at", tickerTime)
		writeEvent(tickerTime)
		time.Sleep(5 * time.Second)
	}
}

// writeEvent creates and event to write
func writeEvent(ticker time.Time) string {
	event := &Event{
		Time:   time.Now(),
		Ticker: ticker,
		Event:  "POLYMER",
	}
	tickerEvent, _ := json.Marshal(event)
	logEventToFile(string(tickerEvent))
	fireEvent()
	return string(tickerEvent)
}

// logEventToFile writes events to an events.log file
func logEventToFile(data string) {
	f, err := os.OpenFile("event.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Println(data)
}

// fireEvent calls out to polymer
func fireEvent() {
	Counter++
	bodyValues := map[string]string{"message": strconv.Itoa(Counter)}
	jsonBody, _ := json.Marshal(bodyValues)
	resp, err := http.Post("http://localhost:8080/api/message", "application/json", bytes.NewBuffer(jsonBody))

	fmt.Println("event " + strconv.Itoa(Counter) + " fired!")
	
	if err != nil {
		fmt.Println("retrying...")
	}
	defer resp.Body.Close()
}
