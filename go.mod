module gitlab.com/jstone28/instrumentation

go 1.13

require (
	github.com/Shopify/sarama v1.26.1
	github.com/klauspost/compress v1.10.0 // indirect
	golang.org/x/crypto v0.0.0-20200210222208-86ce3cb69678 // indirect
)
