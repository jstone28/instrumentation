build:
	go build -o bin/instrumentation main.go

run:
	go run main.go

docker-build:
	docker build -t registry.gitlab.com/jstone28/instrumentation .

docker-push:
	docker push registry.gitlab.com/jstone28/instrumentation

publish: docker-build docker-push

